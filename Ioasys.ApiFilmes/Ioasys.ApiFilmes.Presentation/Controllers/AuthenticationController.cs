﻿using Ioasys.ApiFilmes.Business.Models;
using Ioasys.ApiFilmes.Business.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Ioasys.ApiFilmes.Presentation.Controllers
{
    [AllowAnonymous]
    [Route("Authentication")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        public readonly IAuthService _authService;

        public AuthenticationController(IAuthService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// Manager data of the user
        /// </summary>
        /// <param name="user">Data from user to SignIn</param>
        /// <returns Code="200">Data for signin</returns>
        /// <returns Code="500">Error in process</returns>
        [HttpPost]
        [ProducesResponseType(typeof(UserModel), 200)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public ActionResult SignIn([FromBody] UserModel user) =>
            Ok(_authService.Signin(user));
    }
}
