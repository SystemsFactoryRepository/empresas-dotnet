﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ioasys.ApiFilmes.Business.Services;
using Ioasys.ApiFilmes.Business.Models;
using System.Net;

namespace Ioasys.ApiFilmes.Presentation.Controllers
{
    /// <summary>
    /// Controller inicial
    /// </summary>
    [ApiController]
    [Route("User")]
    public class UserController : ControllerBase
    {
        public readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Obtém todos usuarios
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<UserModel>), 200)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [Route("AllUser")]
        [Authorize(Roles = "administrator")]
        public ActionResult AllUsers() => Ok(_userService.AllUsers());

        /// <summary>
        /// Obtém todos usuarios com paginação
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<UserModel>), 200)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [Route("AllUserPaginated")]
        [Authorize(Roles = "administrator")]
        public ActionResult AllUsersPaginated(int take, int skip) => Ok(_userService.AllUsersPaginated(take, skip));

        /// <summary>
        /// Cadastra usuario
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(List<UserModel>), 200)]
        [ProducesResponseType((int)HttpStatusCode.NotModified)]
        [Route("")]
        [Authorize(Roles = "administrator,client")]
        public ActionResult AddUser(UserModel userModel) => Ok(_userService.User(userModel));

        /// <summary>
        /// Altera os dados do usuario
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(List<UserModel>), 200)]
        [ProducesResponseType((int)HttpStatusCode.NotModified)]
        [Route("")]
        [Authorize(Roles = "administrator,client")]
        public ActionResult EditUser(UserModel userModel) => Ok(_userService.User(userModel));

        /// <summary>
        /// Desativa usuario
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(List<UserModel>), 200)]
        [ProducesResponseType((int)HttpStatusCode.NotModified)]
        [Route("UserClient")]
        [Authorize(Roles = "administrator,client")]
        public ActionResult DisableUser(int id) => Ok(_userService.DisableUser(id));
    }
}
