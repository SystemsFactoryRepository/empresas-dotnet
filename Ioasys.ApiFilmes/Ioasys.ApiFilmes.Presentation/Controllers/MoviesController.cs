﻿using Ioasys.ApiFilmes.Business.Models;
using Ioasys.ApiFilmes.Business.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Ioasys.ApiFilmes.Presentation.Controllers
{
    [Route("Movies")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        public readonly IMovieService _movieService;
        public readonly IParticipantsService _participantsService;

        public MoviesController(IMovieService movieService, IParticipantsService participantsService)
        {
            _movieService = movieService;
            _participantsService = participantsService;
        }

        /// <summary>
        /// Obtem lista de Filmes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<MoviesModel>), 200)]
        [ProducesResponseType((int)HttpStatusCode.NotModified)]
        [Route("")]
        [Authorize(Roles = "administrator")]
        public ActionResult AllMovies() => Ok(_movieService.AllMovies());

        /// <summary>
        /// Cadastra filmes e seus participants
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType((int)HttpStatusCode.NotModified)]
        [Route("")]
        [Authorize(Roles = "administrator")]
        public ActionResult AddMoviesParticipants(MoviesModel model) => Ok(_movieService.InserirMovies(model));
    }
}
