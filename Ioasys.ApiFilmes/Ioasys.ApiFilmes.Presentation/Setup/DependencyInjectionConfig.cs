﻿using AutoMapper;
using Ioasys.ApiFilmes.Business.Services;
using Ioasys.ApiFilmes.Data.Context;
using Ioasys.ApiFilmes.Data.Repositories;
using Ioasys.ApiFilmes.Data.Repositories.Abstract;
using Ioasys.ApiFilmes.Data.Repositories.Concrete;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.ApiFilmes.Presentation.Setup
{
    public static class DependencyInjectionConfig
    {
        public static void RegisterService(this IServiceCollection services)
        {
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IMovieService, MoviesService>();
            services.AddScoped<IParticipantsService, ParticipantsService>();
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IMoviesRepository, MoviesRepository>();
            services.AddScoped<IParticipantsRepository, ParticipantsRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IMapper, Mapper>();

            services.AddScoped<ApplicationDataContext>();
        }
    }
}
