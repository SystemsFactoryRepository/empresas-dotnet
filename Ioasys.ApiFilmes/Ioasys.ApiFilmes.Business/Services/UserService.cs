﻿using AutoMapper;
using Ioasys.ApiFilmes.Business.Models;
using Ioasys.ApiFilmes.Data.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public IQueryable<UserModel> AllUsers()
        {
            var users = _userRepository.AllUsers();

            if (!users.Any())
                return null;

            var userModel = _mapper.ProjectTo<UserModel>(users);

            return userModel;
        }

        public IQueryable<UserModel> AllUsersPaginated(int take, int skip)
        {
            var users = _userRepository.AllUsersPaginated(take, skip);

            if (!users.Any())
                return null;

            var userModel = _mapper.ProjectTo<UserModel>(users);

            return userModel;
        }

        public int User(UserModel userModel)
        {
            var user = _mapper.Map<UserModel, Data.Entities.User>(userModel);
            return _userRepository.User(user);
        }

        public int EditUser(UserModel userModel)
        {
            var user = _mapper.Map<UserModel, Data.Entities.User>(userModel);
            return _userRepository.EditUser(user);
        }

        public int DisableUser(int id)
        {
            return _userRepository.DisableUser(id);
        }
    }
}
