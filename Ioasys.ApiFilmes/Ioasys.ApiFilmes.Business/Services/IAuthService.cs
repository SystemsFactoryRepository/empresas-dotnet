﻿using Ioasys.ApiFilmes.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Services
{
    public interface IAuthService
    {
        DataAuthorizationModel Signin(UserModel userModel);
    }
}
