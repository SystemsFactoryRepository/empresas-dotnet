﻿using AutoMapper;
using Ioasys.ApiFilmes.Business.Models;
using Ioasys.ApiFilmes.Data.Entities;
using Ioasys.ApiFilmes.Data.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Services
{
    public class MoviesService : IMovieService
    {
        private readonly IMoviesRepository _moviesRepository;
        private readonly IParticipantsRepository _participantsRepository;
        private readonly IMapper _mapper;

        public MoviesService(IMoviesRepository moviesRepository, IParticipantsRepository participantsRepository, IMapper mapper)
        {
            _moviesRepository = moviesRepository;
            _participantsRepository = participantsRepository;
            _mapper = mapper;
        }

        public IQueryable<MoviesModel> AllMovies()
        {
            var movies = _moviesRepository.AllMovies();

            if (!movies.Any())
                return null;

            var moviesModel = _mapper.ProjectTo<MoviesModel>(movies);

            return moviesModel;
        }

        public int InserirMovies(MoviesModel moviesModel)
        {
            var movies = _mapper.Map<MoviesModel, Movies>(moviesModel);
            return _moviesRepository.InsertMovies(movies);
        }

        public int EditMovies(MoviesModel moviesModel)
        {
            var movies = _mapper.Map<MoviesModel, Movies>(moviesModel);
            return _moviesRepository.EditMovies(movies);
        }
    }
}
