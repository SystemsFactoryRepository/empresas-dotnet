﻿using Ioasys.ApiFilmes.Business.Models;
using Ioasys.ApiFilmes.Data.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Services
{
    public class AuthService : IAuthService
    {
        public readonly IConfiguration _configuration;
        public readonly IUserService _userService;

        public AuthService(IConfiguration configuration, IUserService userService)
        {
            _configuration = configuration;
            _userService = userService;
        }

        public DataAuthorizationModel Signin(UserModel userModel)
        {
            var user = (from u in _userService.AllUsers().Where(x => x.Email == userModel.Email && x.Password == userModel.Password)
                       select new User() { 
                           Id = u.Id,
                           Name = u.Name,
                           Email = u.Email,
                           Password = u.Password,
                           Roles = u.Roles,
                           FlAtivo = u.FlAtivo
                       }).FirstOrDefault();

            if (user == null || !(user.Email.Equals(userModel.Email) && user.Password.Equals(userModel.Password)))
            {
                //implementação de middleware para tratamento de erro
            }

            return SignInData(user);
        }

        public DataAuthorizationModel SignInData(User user)
        {
            var key = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("Secret"));

            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim("Id", user.Id.ToString()),
                    new Claim("Name", user.Name),
                    new Claim("Email", user.Email),
                    new Claim("Password", user.Password),
                    new Claim(ClaimTypes.Role, user.Roles)
                }),
                Expires = DateTime.UtcNow.AddHours(5),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));

            return new DataAuthorizationModel(user, token);
        }
    }
}
