﻿using AutoMapper;
using Ioasys.ApiFilmes.Business.Models;
using Ioasys.ApiFilmes.Data.Entities;
using Ioasys.ApiFilmes.Data.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Services
{
    public class ParticipantsService : IParticipantsService
    {
        private readonly IParticipantsRepository _participantsRepository;
        private readonly IMapper _mapper;

        public ParticipantsService(IParticipantsRepository participantsRepository, IMapper mapper)
        {
            _participantsRepository = participantsRepository;
            _mapper = mapper;
        }

        public IQueryable<ParticipantsModel> AllParticipants()
        {
            var movies = _participantsRepository.AllParticipants();

            if (!movies.Any())
                return null;

            var moviesModel = _mapper.ProjectTo<ParticipantsModel>(movies);

            return moviesModel;
        }

        public int InserirParticipants(ParticipantsModel participantsModel)
        {
            var participants = _mapper.Map<ParticipantsModel, Participants>(participantsModel);
            return _participantsRepository.InsertParticipants(participants);
        }

        public int EditParticipants(ParticipantsModel participantsModel)
        {
            var participants = _mapper.Map<ParticipantsModel, Participants>(participantsModel);
            return _participantsRepository.EditParticipants(participants);
        }
    }
}
