﻿using Ioasys.ApiFilmes.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Services
{
    public interface IParticipantsService
    {
        IQueryable<ParticipantsModel> AllParticipants();
        int InserirParticipants(ParticipantsModel participantsModel);
        int EditParticipants(ParticipantsModel participantsModel);
    }
}
