﻿using Ioasys.ApiFilmes.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Services
{
    public interface IMovieService
    {
        IQueryable<MoviesModel> AllMovies();
        int InserirMovies(MoviesModel moviesModel);
        int EditMovies(MoviesModel moviesModel);
    }
}
