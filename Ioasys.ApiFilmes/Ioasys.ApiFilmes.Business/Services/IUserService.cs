﻿using Ioasys.ApiFilmes.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Services
{
    public interface IUserService
    {
        IQueryable<UserModel> AllUsers();
        IQueryable<UserModel> AllUsersPaginated(int take, int skip);
        int User(UserModel userModel);
        int EditUser(UserModel userModel);
        int DisableUser(int id);
    }
}
