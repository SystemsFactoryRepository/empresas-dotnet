﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Models
{
    public class ParticipantsMoviesModel
    {
        public int ParticipantId { get; set; }
        public ParticipantsModel Participants { get; set; }
        public int MoviesId { get; set; }
        public MoviesModel Movies { get; set; }
    }
}
