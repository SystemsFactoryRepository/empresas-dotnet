﻿using Ioasys.ApiFilmes.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Models
{
    public class DataAuthorizationModel
    {
        public UserModel User { get; set; }
        public string Token { get; set; }

        public DataAuthorizationModel(User user, string token)
        {
            User = new UserModel();
            User.Id = user.Id;
            User.Email = user.Email;
            User.Password = user.Password;
            User.Roles = user.Roles;
            Token = token;
        }
    }
}
