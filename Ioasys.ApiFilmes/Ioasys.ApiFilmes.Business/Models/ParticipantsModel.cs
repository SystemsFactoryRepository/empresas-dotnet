﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Models
{
    public class ParticipantsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
