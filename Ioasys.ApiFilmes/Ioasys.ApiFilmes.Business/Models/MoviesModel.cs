﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.ApiFilmes.Business.Models
{
    public class MoviesModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }

        public List<ParticipantsModel> ParticipantsModel { get; set; } = 
            new List<ParticipantsModel>();
    }
}
