﻿using AutoMapper;
using Ioasys.ApiFilmes.Business.Models;
using Ioasys.ApiFilmes.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.ApiFilmes.Business.AutoMapperSetup
{
    public class EntityToModelMapping : Profile
    {
        public EntityToModelMapping()
        {
            CreateMap<User, UserModel>();
            CreateMap<UserModel, User>();

            CreateMap<Movies, MoviesModel>()
                .ForMember(x => x.ParticipantsModel, opt => opt.Ignore());
            CreateMap<MoviesModel, Movies>()
                .ForMember(x => x.ParticipantsMovies, opt => opt.Ignore());

            CreateMap<Participants, ParticipantsModel>();
            CreateMap<ParticipantsModel, Participants>()
                .ForMember(x => x.MoviesParticipants, opt => opt.Ignore());
        }
    }
}
