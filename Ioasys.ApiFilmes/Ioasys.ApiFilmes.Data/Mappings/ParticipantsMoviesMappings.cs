﻿using Ioasys.ApiFilmes.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Mappings
{
    public class ParticipantsMoviesMappings : IEntityTypeConfiguration<ParticipantsMovies>
    {
        public void Configure(EntityTypeBuilder<ParticipantsMovies> builder)
        {
            builder.HasKey(k => new { k.MoviesId, k.ParticipantId });

            builder.HasData(ReadJsonData());

            builder.ToTable("ParticipantsMovies");
        }

        public ParticipantsMovies[] ReadJsonData()
        {
            using (StreamReader reader = new StreamReader("..\\Ioasys.ApiFilmes.Data\\JsonData\\ParticipantsMovies.json"))
            {
                return JsonConvert.DeserializeObject<ParticipantsMovies[]>(reader.ReadToEnd());
            }
        }
    }
}
