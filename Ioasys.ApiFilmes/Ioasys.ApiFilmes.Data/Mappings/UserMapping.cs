﻿using Ioasys.ApiFilmes.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Mappings
{
    public class UserMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                .HasColumnType("VARCHAR(50)")
                .IsRequired();

            builder.Property(x => x.Email)
                .HasColumnType("VARCHAR(50)")
                .IsRequired();

            builder.Property(x => x.Password)
                .HasColumnType("VARCHAR(50)")
                .IsRequired();

            builder.Property(x => x.Roles)
                .HasColumnType("VARCHAR(50)")
                .IsRequired();

            builder.Property(x => x.FlAtivo)
                .HasColumnType("INTEGER")
                .IsRequired();

            builder.ToTable("User");

            builder.HasData(ReadJsonData());
        }

        private User[] ReadJsonData()
        {
            using (StreamReader reader = new StreamReader("..\\Ioasys.ApiFilmes.Data\\JsonData\\User.json"))
            {
                return JsonConvert.DeserializeObject<User[]>(reader.ReadToEnd());
            }
        }
    }
}
