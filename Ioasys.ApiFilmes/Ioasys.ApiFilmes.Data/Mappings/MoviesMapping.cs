﻿using Ioasys.ApiFilmes.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Mappings
{
    public class MoviesMapping : IEntityTypeConfiguration<Movies>
    {
        public void Configure(EntityTypeBuilder<Movies> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                .HasColumnType("VARCHAR(50)")
                .IsRequired();

            builder.Property(x => x.Genre)
                .HasColumnType("VARCHAR(50)")
                .IsRequired();

            builder.Ignore("ParticipantsMovies");
            builder.HasData(ReadJsonData());
            builder.ToTable("Movies");
        }

        private Movies[] ReadJsonData()
        {
            using (StreamReader reader = new StreamReader("..\\Ioasys.ApiFilmes.Data\\JsonData\\Movies.json"))
            {
                return JsonConvert.DeserializeObject<Movies[]>(reader.ReadToEnd());
            }
        }
    }
}
