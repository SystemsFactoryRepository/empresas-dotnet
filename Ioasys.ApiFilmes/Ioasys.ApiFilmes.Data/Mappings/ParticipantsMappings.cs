﻿using Ioasys.ApiFilmes.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Mappings
{
    public class ParticipantsMappings : IEntityTypeConfiguration<Participants>
    {
        public void Configure(EntityTypeBuilder<Participants> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                .HasColumnType("VARCHAR(50)")
                .IsRequired();

            builder.Property(x => x.Type)
                .HasColumnType("VARCHAR(50)")
                .IsRequired();

            builder.Ignore("MoviesParticipants");
            builder.HasData(ReadJsonData());
            builder.ToTable("Participants");
        }

        private Participants[] ReadJsonData()
        {
            using (StreamReader reader = new StreamReader("..\\Ioasys.ApiFilmes.Data\\JsonData\\Participants.json"))
            {
                return JsonConvert.DeserializeObject<Participants[]>(reader.ReadToEnd());
            }
        }
    }
}
