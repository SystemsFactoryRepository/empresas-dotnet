﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Entities
{
    public class ParticipantsMovies
    {
        public int ParticipantId { get; set; }
        public Participants Participants { get; set; }
        public int MoviesId { get; set; }
        public Movies Movies { get; set; }
    }
}
