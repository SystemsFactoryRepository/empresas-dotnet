﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Entities
{
    public class Movies
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }

        public ICollection<ParticipantsMovies> ParticipantsMovies { get; set; } = new List<ParticipantsMovies>();
    }
}
