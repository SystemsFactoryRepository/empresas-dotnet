﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Entities
{
    public class Participants
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

        public ICollection<ParticipantsMovies> MoviesParticipants { get; set; } = new List<ParticipantsMovies>();
    }
}
