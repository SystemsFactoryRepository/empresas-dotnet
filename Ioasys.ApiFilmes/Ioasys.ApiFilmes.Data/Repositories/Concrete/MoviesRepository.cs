﻿using Ioasys.ApiFilmes.Data.Entities;
using Ioasys.ApiFilmes.Data.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Repositories.Concrete
{
    public class MoviesRepository : IMoviesRepository
    {
        private readonly IBaseRepository<Movies> _moviesRepository;

        public MoviesRepository(IBaseRepository<Movies> moviesRepository)
        {
            _moviesRepository = moviesRepository;
        }

        public IQueryable<Movies> AllMovies()
        {
            return _moviesRepository.All();
        }

        public int InsertMovies(Movies movies)
        {
            _moviesRepository.Add(movies);

            return _moviesRepository.SaveChange();
        }

        public int EditMovies(Movies movies)
        {
            _moviesRepository.Edit(movies);

            return _moviesRepository.SaveChange();
        }
    }
}
