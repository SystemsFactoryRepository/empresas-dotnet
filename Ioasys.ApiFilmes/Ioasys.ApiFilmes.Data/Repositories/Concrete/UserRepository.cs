﻿using Ioasys.ApiFilmes.Data.Entities;
using Ioasys.ApiFilmes.Data.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Repositories.Concrete
{
    public class UserRepository : IUserRepository
    {
        private readonly IBaseRepository<User> _userRepository;

        public UserRepository(IBaseRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        public IQueryable<User> AllUsers()
        {
            return _userRepository.All();
        }

        public IQueryable<User> AllUsersPaginated(int take, int skip)
        {
            return _userRepository.All().Where(x => x.Roles != "client")
                .Skip(skip).Take(take).OrderBy(x => x.Name);
        }

        public int User(User user)
        {
            _userRepository.Add(user);

            return _userRepository.SaveChange();
        }

        public int DisableUser(int id)
        {
            var user = _userRepository.All().Where(x => x.Id == id).FirstOrDefault();
            user.FlAtivo = 0;
            _userRepository.Edit(user);

            return _userRepository.SaveChange();
        }

        public int EditUser(User user)
        {
            _userRepository.Edit(user);

            return _userRepository.SaveChange();
        }
    }
}
