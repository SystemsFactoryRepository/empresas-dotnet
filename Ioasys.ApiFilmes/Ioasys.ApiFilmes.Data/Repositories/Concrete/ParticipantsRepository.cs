﻿using Ioasys.ApiFilmes.Data.Entities;
using Ioasys.ApiFilmes.Data.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Repositories.Concrete
{
    public class ParticipantsRepository : IParticipantsRepository
    {
        private readonly IBaseRepository<Participants> _participantsRepository;

        public ParticipantsRepository(IBaseRepository<Participants> participantsRepository)
        {
            _participantsRepository = participantsRepository;
        }

        public IQueryable<Participants> AllParticipants()
        {
            return _participantsRepository.All();
        }

        public int InsertParticipants(Participants participants)
        {
            _participantsRepository.Add(participants);

            return _participantsRepository.SaveChange();
        }

        public int EditParticipants(Participants participants)
        {
            _participantsRepository.Edit(participants);

            return _participantsRepository.SaveChange();
        }
    }
}
