﻿using Ioasys.ApiFilmes.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Repositories.Abstract
{
    public interface IUserRepository
    {
        IQueryable<User> AllUsers();
        IQueryable<User> AllUsersPaginated(int take, int skip);
        int User(User user);
        int DisableUser(int id);
        int EditUser(User user);
    }
}
