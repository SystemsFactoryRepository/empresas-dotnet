﻿using Ioasys.ApiFilmes.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Repositories.Abstract
{
    public interface IParticipantsRepository
    {
        IQueryable<Participants> AllParticipants();

        int InsertParticipants(Participants participants);

        int EditParticipants(Participants participants);
    }
}
