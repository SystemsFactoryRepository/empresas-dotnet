﻿using Ioasys.ApiFilmes.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Repositories.Abstract
{
    public interface IMoviesRepository
    {
        IQueryable<Movies> AllMovies();

        int InsertMovies(Movies movies);

        int EditMovies(Movies movies);
    }
}
