﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Repositories
{
    public interface IBaseRepository<T>
    {
        IQueryable<T> All();
        IQueryable<T> RequestByExpression(Expression<Func<T, bool>> predicate);
        void Add(T obj);
        void Edit(T obj);
        void Delete(T obj);
        int SaveChange();
        void Dispose();
    }
}
