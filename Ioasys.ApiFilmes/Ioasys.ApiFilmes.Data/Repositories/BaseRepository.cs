﻿using Ioasys.ApiFilmes.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Ioasys.ApiFilmes.Data.Repositories
{
    public class BaseRepository<T> : IDisposable, IBaseRepository<T> where T : class
    {
        private readonly ApplicationDataContext _context;
        private readonly DbSet<T> _dbSet;

        public BaseRepository(ApplicationDataContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public virtual IQueryable<T> All() => _dbSet.AsNoTracking();

        public virtual IQueryable<T> RequestByExpression(Expression<Func<T, bool>> predicate) =>
            _dbSet.AsNoTracking().Where(predicate);

        public virtual void Add(T obj) => _dbSet.Add(obj);

        public virtual void Edit(T obj) => _dbSet.Update(obj);

        public virtual void Delete(T obj) => _dbSet.Remove(obj);

        public int SaveChange() => _context.SaveChanges();

        public void Dispose() => _context.Dispose();
    }
}
