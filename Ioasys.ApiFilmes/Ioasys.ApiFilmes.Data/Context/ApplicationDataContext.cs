﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Ioasys.ApiFilmes.Data.Entities;

namespace Ioasys.ApiFilmes.Data.Context
{
    public class ApplicationDataContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<Movies> Movies { get; set; }
        public DbSet<Participants> Participants { get; set; }
        public DbSet<ParticipantsMovies> ParticipantsMovies { get; set; }

        public ApplicationDataContext(DbContextOptions<ApplicationDataContext> options) : base(options){}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDataContext).Assembly);

            base.OnModelCreating(modelBuilder);
        }
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ApplicationDataContext>
    {
        public ApplicationDataContext CreateDbContext(string[] args)
        {
            IConfigurationRoot config =
                new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(@Directory.GetCurrentDirectory() + "/../Ioasys.ApiFilmes.Presentation/appsettings.json").Build();

            var builder = new DbContextOptionsBuilder<ApplicationDataContext>();

            var connectionString = config.GetConnectionString("DefaultConnection");
            builder.UseSqlServer(connectionString);
            return new ApplicationDataContext(builder.Options);
        }
    }
}
