﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ioasys.ApiFilmes.Data.Migrations
{
    public partial class NewMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    Genre = table.Column<string>(type: "VARCHAR(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Participants",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    Type = table.Column<string>(type: "VARCHAR(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Participants", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    Email = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    Password = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    Roles = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    FlAtivo = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParticipantsMovies",
                columns: table => new
                {
                    ParticipantId = table.Column<int>(type: "int", nullable: false),
                    MoviesId = table.Column<int>(type: "int", nullable: false),
                    ParticipantsId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParticipantsMovies", x => new { x.MoviesId, x.ParticipantId });
                    table.ForeignKey(
                        name: "FK_ParticipantsMovies_Movies_MoviesId",
                        column: x => x.MoviesId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ParticipantsMovies_Participants_ParticipantsId",
                        column: x => x.ParticipantsId,
                        principalTable: "Participants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Genre", "Name" },
                values: new object[,]
                {
                    { 1, "Drama", "As Tran�as do Careca" },
                    { 2, "Suspense", "A Volta dos que n�o foram" }
                });

            migrationBuilder.InsertData(
                table: "Participants",
                columns: new[] { "Id", "Name", "Type" },
                values: new object[,]
                {
                    { 1, "Jarbas", "actor" },
                    { 2, "Mafalda", "director" }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "Email", "FlAtivo", "Name", "Password", "Roles" },
                values: new object[,]
                {
                    { 1, "tiao@email.com", 1, "Ti�o", "123", "administrator" },
                    { 2, "tirisnelda@email.com", 1, "Tirisnelda", "123", "client" },
                    { 3, "epaminondas@email.com", 1, "Epaminondas", "123", "client" }
                });

            migrationBuilder.InsertData(
                table: "ParticipantsMovies",
                columns: new[] { "MoviesId", "ParticipantId", "ParticipantsId" },
                values: new object[] { 1, 1, null });

            migrationBuilder.InsertData(
                table: "ParticipantsMovies",
                columns: new[] { "MoviesId", "ParticipantId", "ParticipantsId" },
                values: new object[] { 1, 2, null });

            migrationBuilder.CreateIndex(
                name: "IX_ParticipantsMovies_ParticipantsId",
                table: "ParticipantsMovies",
                column: "ParticipantsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ParticipantsMovies");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Participants");
        }
    }
}
